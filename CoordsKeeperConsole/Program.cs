﻿namespace CoordsKeeperConsole
{
    using CommandLine;
    #region Usings

    using System;
    using System.IO;

    #endregion

    /// <summary>
    /// The main program class.
    /// </summary>
    public class Program
    {
        #region Methods

        /// <summary>
        /// The main method.
        /// </summary>
        /// <param name="args">
        /// The command line arguments.
        /// </param>
        static void Main(string[] args)
        {
            ParserResult<ConfigurationHandler> results = Parser.Default.ParseArguments<ConfigurationHandler>(args);
            var test = results.WithParsed<ConfigurationHandler>(cfg => Worker(cfg));

        }

        /// <summary>
        /// The worker method.
        /// </summary>
        /// <param name="configuration">
        /// The configuration.
        /// </param>
        static void Worker(ConfigurationHandler configuration)
        {
            var dict = configuration.GetConfigurationFromFile();

            DatabaseHandler databaseHandler = new DatabaseHandler(dict["ConnectionString"], dict["DatabaseName"], dict["CollectionName"]);

            databaseHandler.Insert(new CoordsInfo
            {
                Coords = "eh",
                Info = "banana",
                IsLooted = true,
                Name = "Portal",
                Realm = "Overworld"
            }).Wait();

            var list = databaseHandler.Select<CoordsInfo>().Result;
            var item = databaseHandler.Select<CoordsInfo>(list[0].Id).Result;

            databaseHandler.Remove<CoordsInfo>(list[0].Id).Wait();

            Console.ReadLine();
        }

        #endregion
    }
}