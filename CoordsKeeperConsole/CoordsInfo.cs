﻿namespace CoordsKeeperConsole
{
    #region Usings

    using MongoDB.Bson;

    #endregion

    /// <summary>
    /// The coord info class.
    /// </summary>
    public class CoordsInfo
    {
        #region Properties and Indexers

        /// <summary>
        /// Gets or sets the ID.
        /// </summary>
        public ObjectId Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the coordinates.
        /// </summary>
        public string Coords { get; set; }

        /// <summary>
        /// Gets or sets the realm.
        /// </summary>
        public string Realm { get; set; }

        /// <summary>
        /// Gets or sets the is looted parameter.
        /// </summary>
        public bool IsLooted { get; set; }

        /// <summary>
        /// Gets or sets the information.
        /// </summary>
        public string Info { get; set; }

        #endregion
    }
}