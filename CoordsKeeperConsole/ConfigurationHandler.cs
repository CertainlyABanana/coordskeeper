﻿namespace CoordsKeeperConsole
{
    #region Usings

    using System;
    using System.Collections.Generic;
    using System.Xml;

    using CommandLine;

    #endregion

    /// <summary>
    /// The configuration handler class.
    /// </summary>
    public class ConfigurationHandler
    {
        #region Fields and constants

        /// <summary>
        /// The main XML node name.
        /// </summary>
        private const string MainXmlNodeName = "Database";

        #endregion

        #region Properties and Indexers

        /// <summary>
        /// Gets or sets the filepath.
        /// </summary>
        [Option('c', "configuration", Required = true, HelpText = "Path to XML configuration file.")]
        public string Filepath { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// The get configuration from XML file
        /// </summary>
        /// <param name="filepath">
        /// The path to the XML file.
        /// </param>
        /// <returns>
        /// Dictionary containing loaded configuration.
        /// </returns>
        public Dictionary<string, string> GetConfigurationFromFile()
        {
            Dictionary<string, string> expectedConfiguration = new Dictionary<string, string>();
            List<string> expectedNodes = new List<string>()
            {
                "ConnectionString",
                "DatabaseName",
                "CollectionName"  
            };

            XmlReaderSettings settings = new XmlReaderSettings()
            {
                IgnoreComments = true,
                IgnoreWhitespace = true,
                IgnoreProcessingInstructions = true
            };

            using (XmlReader reader = XmlReader.Create(Filepath, settings))
            {
                // skip XML declaration
                reader.MoveToContent();
                reader.ReadStartElement(MainXmlNodeName);

                // read expected nodes
                try
                {
                    foreach(var item in expectedNodes)
                    {
                        expectedConfiguration.Add(item, reader.ReadElementContentAsString(item, ""));
                    }
                } 
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return expectedConfiguration;
        }

        #endregion
    }
}