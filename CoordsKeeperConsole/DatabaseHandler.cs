﻿namespace CoordsKeeperConsole
{

    #region Usings

    using System.Collections.Generic;
    using System.Threading.Tasks;

    using MongoDB.Bson;
    using MongoDB.Driver;

    #endregion

    /// <summary>
    /// The database handler class.
    /// </summary>
    public class DatabaseHandler
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes instance of <see cref="DatabaseHandler"/> class.
        /// </summary>
        public DatabaseHandler(string connectionString, string databaseName, string collectionName)
        {
            this.ConnectionString = connectionString;
            this.DatabaseName = databaseName;
            this.CollectionName = collectionName;
        }

        #endregion

        #region Properties and Indexers

        /// <summary>
        /// Gets or sets the connection string for database.
        /// </summary>
        private string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets the database name.
        /// </summary>
        private string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets collection name.
        /// </summary>
        private string CollectionName { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Inserts record to database.
        /// </summary>
        /// <typeparam name="T">
        /// Type of single database record.
        /// </typeparam>
        /// <param name="data">
        /// Data to be inserted
        /// </param>
        /// <returns>
        /// <see cref="Task"/>
        /// </returns>
        public async Task Insert<T>(T data)
        {
            var client = new MongoClient(this.ConnectionString);

            IMongoDatabase db = client.GetDatabase(this.DatabaseName);
            IMongoCollection<T> collection = db.GetCollection<T>(this.CollectionName);

            await collection.InsertOneAsync(data);
        }

        /// <summary>
        /// Selects all records from database.
        /// </summary>
        /// <typeparam name="T">
        /// Type of single database record.
        /// </typeparam>
        /// <returns>
        /// <see cref="Task"/>
        /// </returns>
        public async Task<List<T>> Select<T>()
        {
            var client = new MongoClient(this.ConnectionString);

            IMongoDatabase db = client.GetDatabase(this.DatabaseName);
            IMongoCollection<T> collection = db.GetCollection<T>(this.CollectionName);

            return await collection.Find(Builders<T>.Filter.Empty).ToListAsync();
        }

        /// <summary>
        /// Selects one record by ID from database.
        /// </summary>
        /// <typeparam name="T">
        /// Type of single database record.
        /// </typeparam>
        /// <param name="id">
        /// The ID of record to be found.
        /// </param>
        /// <returns>
        /// <see cref="Task"/>
        /// </returns>
        public async Task<T> Select<T>(ObjectId id)
        {
            var client = new MongoClient(this.ConnectionString);

            IMongoDatabase db = client.GetDatabase(this.DatabaseName);
            IMongoCollection<T> collection = db.GetCollection<T>(this.CollectionName);
            FilterDefinition<T> filter = Builders<T>.Filter.Eq("Id", id);

            return await collection.Find(filter).FirstAsync<T>();
        }

        /// <summary>
        /// Removes one record from database.
        /// </summary>
        /// <typeparam name="T">
        /// Type of single database record.
        /// </typeparam>
        /// <param name="id">
        /// The ID of record to be removed.
        /// </param>
        /// <returns>
        /// <see cref="Task"/>
        /// </returns>
        public async Task Remove<T>(ObjectId id)
        {
            var client = new MongoClient(this.ConnectionString);

            IMongoDatabase db = client.GetDatabase(this.DatabaseName);
            IMongoCollection<T> collection = db.GetCollection<T>(this.CollectionName);
            FilterDefinition<T> filter = Builders<T>.Filter.Eq("Id", id);

            await collection.DeleteOneAsync(filter);
        }

        /// <summary>
        /// Updates one record in database.
        /// </summary>
        /// <typeparam name="T">
        /// Type of single database record.
        /// </typeparam>
        /// <param name="id">
        /// The ID of record to be updated.
        /// </param>
        /// <returns>
        /// <see cref="Task"/>
        /// </returns>
        public async Task Update<T, P>(ObjectId id, string fieldName, P fieldValue)
        {
            var client = new MongoClient(this.ConnectionString);

            IMongoDatabase db = client.GetDatabase(this.DatabaseName);
            IMongoCollection<T> collection = db.GetCollection<T>(this.CollectionName);
            FilterDefinition<T> filter = Builders<T>.Filter.Eq("Id", id);
            UpdateDefinition<T> update = Builders<T>.Update.Set<P>(fieldName, fieldValue);

            await collection.UpdateOneAsync(filter, update);
        }

        #endregion
    }
}